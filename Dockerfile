FROM registry.gitlab.com/naonobu.nakashima/docker_demo/docker_build:0.1

LABEL description="Dockerfile demo"

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .
CMD [ "python", "./client.py" ]
