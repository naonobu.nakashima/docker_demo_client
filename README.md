# Outline

* The container created from the Dockerfile execute the python program which can send own container cpu & mem usage to the sever. 

# How to use

1. Git clone.
    ~~~
    > git clone https://gitlab.com/naonobu.nakashima/docker_demo_client.git
    ~~~
2. Build docker Image from Dockerfile.
    ~~~
    > docker image build -t [Registory]:[tag] .
    ~~~
3. Docker run.
   1. Be sure to configure Docker DNS settings to be able to resolve the hostname"YOUR_SERVER_NAME" below on your docker container. 
   2. Docker run, then the python program on the container send cpu & mem usage own to server set in SERVER_HOSTNAME.
       ~~~
       > docker run --env SERVER_HOSTNAME=YOUR_SERVER_NAME --rm [Registory]:[tag]
       ~~~