import os
import csv
import requests
import psutil
import time
import json
from pprint import pprint
from datetime import datetime
from pytz import timezone

class SupportJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        return super(DateTimeSupportJSONEncoder, self).default(o)


url_items = 'http://' + os.environ['SERVER_HOSTNAME'] + ':5000/usage'
headers = {
    'Content-type': 'application/json'
}

hostname=os.uname()[1]

for i in range(0, 10000, 1):

    time.sleep(1)

    # time
    t = datetime.now(timezone('Asia/Tokyo')).strftime('%Y-%m-%d %H:%M:%S') 
    
    # cpu
    cpu = psutil.cpu_percent(interval=1)

    # mem
    mem = psutil.virtual_memory().percent

    item_data = {
        'time': t,
        'hostname': hostname,
        'cpu': cpu,
        'mem': mem
    }
    
    r_post =requests.post(url_items, headers=headers, json=json.dumps(item_data, cls=SupportJSONEncoder), verify=False)

    pprint(
        {
            "status_code": r_post.status_code,
            "json": r_post.json()
        }
    )

    with open('./client.log','a')as f:
        pprint(
            {
                "status_code": r_post.status_code,
                "json": r_post.json()
            },
            width=2000,
            stream=f
            )